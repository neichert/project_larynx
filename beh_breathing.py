'''derive the breathing traces from biopac data and plot the traces (Figure 1)'''

import os
import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import json
import pdb
import pandas as pd
import sys
from scipy.signal import butter, lfilter, freqz
from matplotlib.ticker import FormatStrFormatter

rootdir = '/myDir/project_larynx'

# Load subject info
subs = pd.read_csv(os.path.join(rootdir, 'participants.tsv'), sep='\t', dtype={'id': str})

# script settings
do_plot = 0 # set to 1 for individual analysis (has to be done before group plot)
do_group_plot = 1

# sampling rate of biopac
fs = 500
# TR of scanner (to determine onset of task)
TR = 0.8
# volumes to discard at the beginning of a scan
n_discard = 25
# time that a block lasts for croppign
t_block = 30
samples_condition = t_block * fs

if do_group_plot:
    print("make group plot")
    for task in 'ArtVoc', 'Factorial':
        # task-specific labels for the blocks
        if task == 'ArtVoc':
            task_code = 1
            lables = ['tongue', 'lip', 'vowel', 'breathe only']
        elif task == 'Factorial':
            task_code = 2
            lables = ['overt speech', 'silent mouthing', 'vowel', 'think']

        task_segments = pd.DataFrame(columns=('id', 'task_code', 'condition', 'seg'))

        for ind, sub_row in subs.iterrows():
            # load in the traces that were stored for this subject before and concatenate
            print('add subject ' + sub_row.id + ', task ' + task + ' ...')
            segments_sub_task = pd.read_pickle(os.path.join(rootdir,
                                               'derivatives',
                                               'sub-' + sub_row.id,
                                               'beh',
                                               'sub-' + sub_row.id + '_' + task + '_breathing.pkl'))
            task_segments = pd.concat([task_segments, segments_sub_task], ignore_index=True, sort=False)

        # initialize figure
        plt.figure(figsize=(20, 10))

        # loop over conditions
        for con in set(task_segments.condition):
            plt.subplot(2, 2, con)
            plt.title(lables[con - 1], fontsize=20)
            # x-tick spacing 5 s
            plt.xticks(np.linspace(0, samples_condition, 7, dtype=int), (np.linspace(0, samples_condition, 7) / fs).astype(int), fontsize=20)
            plt.yticks([])
            # vetrical line after 22 s
            plt.axvline(x=22 * fs, color='black', linestyle='--')

            # loop over subjects
            for sub, sub_row in subs.iterrows():
                # derive numerical values of segment
                a = task_segments['seg'].loc[(task_segments['condition'] == con) & (task_segments['id'] == sub_row.id)].values
                my_matrix_sub = np.array(a[0])
                # loop over repetitions for condition and add to matrix
                for i in range(1, a.shape[0]):
                    new_row = np.append(np.array(a[i]), np.empty((1, samples_condition - len(np.array(a[i])))) * np.nan)
                    new_row = new_row / max(new_row)
                    my_matrix_sub = np.vstack([my_matrix_sub, new_row])

                # add subject average for condition to plot
                my_mean_sub = np.nanmean(my_matrix_sub, axis=0)
                plt.plot(my_mean_sub, color='darkgray', linewidth=1)

                # append matrix with subject average
                if sub_row.id == min(subs.id):
                    my_matrix = my_mean_sub
                else:
                    my_matrix = np.vstack([my_matrix, my_mean_sub])

            # add grant mean to plot
            my_mean = np.nanmean(my_matrix, axis=0)
            plt.plot(my_mean, color='black', linewidth=2)

        if do_plot:
            plt.show()
        else:
            print("save group figure")
            plt.savefig(os.path.join(myscratch, 'LarynxRepresentation', 'derivatives', 'group', 'beh', 'group_' + task + '_breathing_lines.png'))
            plt.close()
    # if group plot is derived, don't continue with individual analysis

# if no group analysis, do breathing analysis for individual subjects
else:
    print("process individual subjects")

    # define sub-functions for filtering
    def butter_lowpass(cutoff, fs, order=5):
        nyq = 0.5 * fs
        normal_cutoff = cutoff / nyq
        b, a = butter(order, normal_cutoff, btype='low', analog=False)
        return b, a

    def butter_lowpass_filter(data, cutoff, fs, order=5):
        b, a = butter_lowpass(cutoff, fs, order=order)
        y = lfilter(b, a, data)
        return y

    my_max_global = 0
    my_min_global = 0

    for ind, sub_row in subs.iterrows():
        # output directory
        OD = os.path.join(rootdir, 'derivatives', 'sub-' + sub_row.id)

        # Read biopac data
        biopac_file = scipy.io.loadmat(os.path.join(rootdir, 'sub-' + sub_row.id,
            'beh', 'biopac_sub-' + sub_row.id + '.mat'))

        biopac_data = np.array(biopac_file['data'])

        # low-pass filter
        biopac_data[:, 1] = butter_lowpass_filter(biopac_data[:, 1], 50, fs, 6)

        # look at scanner pulses to determine on and offset for task manually
        if do_plot:
            plt.plot(biopac_data[:, 2])
            plt.title('subject ' + sub_row.id)
            plt.show()

        # wait here so that numbers for samples can be entered in participants.tsv file

        for task in 'ArtVoc', 'Factorial':
            print ('do subject ' + sub_row.id + ', task ' + task + ' ...')

            segments_sub_task = pd.DataFrame(columns=('id', 'task_code', 'condition', 'seg'))

            # task specific settings
            if task == 'ArtVoc':
                start_sample = sub_row.start_ArtVoc
                stop_sample = sub_row.stop_ArtVoc
                # order of conditions
                rand_list = [1, 2, 3, 4, 2, 4, 1, 3, 4, 3, 2, 1, 3, 1, 4, 2]
                n_blocks = 16
                task_code = 1
                lables = ['tongue', 'lip', 'vowel', 'breathe']

            elif task == 'Factorial':
                start_sample = sub_row.start_Factorial
                stop_sample = sub_row.stop_Factorial
                # order of conditions
                rand_list = [1, 2, 3, 4, 2, 1, 4, 3, 2, 4, 1, 3, 4, 3, 2, 1, 3, 1, 4, 2]
                n_blocks = 20
                task_code = 2
                lables = ['say', 'mouth', 'phon', 'think']

            # get slice timing
            json_file = open(os.path.join(rootdir, 'sub-' + sub_row.id,
                'func', 'sub-' + sub_row.id + '_task-' + task + '_bold.json'))

            json_data = json.load(json_file)
            slice_timing = json_data['SliceTiming']
            np.savetxt(os.path.join(OD, 'beh', 'pnm', 'sub-' + sub_row.id + '_' + task + '_slices.txt'), slice_timing, fmt='%1.4f')

            # Get pnm-data roughly cropped for the whole task
            cut = biopac_data[range(start_sample, stop_sample), :]
            if do_plot:
                plt.plot(cut[:, 2])
                plt.title('inspect segment ' + task)
                plt.show()

            # determine booleian vector when scanner triggers were sent
            scanner_on = cut[:, 2] == 5
            scanner_on = np.diff(scanner_on)
            scanner_on = np.append(scanner_on, [False])

            # exact samples of first and last scanner pulse
            # first and last are indices counted from 'start_sample'
            first = np.where(scanner_on == 1)[0][0]
            last = np.where(scanner_on == 1)[0][-1]

            # crop data from first to last scanner pulse
            biopac_data_range = cut[first:last, :]
            pnm_data = biopac_data[start_sample + first + 1:start_sample + last + 1]
            np.savetxt(os.path.join(OD, 'beh', 'pnm', 'sub-' + sub_row.id + '_' + task + '_pnm.txt'), pnm_data, fmt='%1.3f')

            # Get breathing-EV
            # get breathing column values for each TR
            breath = cut[scanner_on, 1]
            breath = breath[0::2]
            breath = breath[n_discard::]

            # de-mean
            breath_demean = breath - np.mean(breath)
            if do_plot:
                plt.plot(breath_demean)
                plt.title('breathing trace')
                plt.show()

            # save breathing EV #
            np.savetxt(os.path.join(OD, 'beh', 'pnm', 'sub-' + sub_row.id + '_' + task + '_breath.txt'), breath_demean, fmt='%1.3f')

            # Make individual subject figure for breathing
            my_max = 0
            my_min = 0
            for i_block in range(0, n_blocks):
                con = rand_list[i_block]
                seg_start = int(n_discard * TR * fs) + i_block * samples_condition
                seg = pnm_data[seg_start:seg_start + samples_condition, 1]
                seg = list(seg - np.mean(seg))
                # store segments in df
                segments_sub_task.loc[i_block] = [sub_row.id, task_code, con, seg]
                my_max = max(seg) if max(seg) > my_max else my_max
                my_min = min(seg) if min(seg) < my_min else my_min
                my_max_global = max(seg) if max(seg) > my_max_global else my_max_global
                my_min_global = min(seg) if min(seg) < my_min_global else my_min_global

            plt.figure()
            plt.title('subject' + sub_row.id)
            for con in set(rand_list):
                plt.subplot(2, 2, con)
                plt.title(lables[con - 1])
                plt.ylim([my_min, my_max])
                for row in segments_sub_task.loc[(segments_sub_task['task_code'] == task_code) & (segments_sub_task['condition'] == con) & (segments_sub_task['id'] == sub_row.id)].seg.iteritems():
                    plt.plot(row[1])

            if do_plot:
                plt.show()

            print("save individual figure")
            plt.savefig(os.path.join(OD, 'beh', 'sub-' + sub_row.id + '_' + task + '_breathing.png'))
            plt.close()

            # fixes: sub-15
            if (sub_row.id == '15' and task_code == 2):
                # exclude trace with jumps
                ex = segments_sub_task[(segments_sub_task['task_code'] == 2) & (segments_sub_task['condition'] == 3) & (segments_sub_task['id'] == '15')].index[0]
                segments_sub_task = segments_sub_task.drop(ex)

            # save individual segments
            segments_sub_task.to_pickle(os.path.join(OD, 'beh', 'sub-' + sub_row.id + '_' + task + '_breathing.pkl'))
