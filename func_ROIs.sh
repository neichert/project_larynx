'''extract individual fMRI peaks (Figure 4A).
Call for example as: bash func_ROIs.sh 01 Factorial 7 L dorsal'''

rootdir='/myDir/project_larynx'
subj_list='01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20'
GD=$rootdir/derivatives/group

subj=$1
OD=$rootdir/derivatives/sub-${subj}
# task: Factorial (syllable production task, task 1) 
# or ArtVoc (basic localizer task, task 2) 
task=$2
# cp: FEAT task contrast
cp=$3
# hemi: hemisphere
hemi=$4
# which ROI
ROI=$5
# size of sphere to exclude from tongue contrast
tongue_kernel=7

case $hemi in
    L)
    structure=CORTEX_LEFT
    ;;
    R)
    structure=CORTEX_RIGHT
    ;;
esac

# -----
# colour settings
# -----
# tongue
if [ ${task} = 'ArtVoc' ] && [[ ${cp} = "5" ]] ; then
  colour=fsl_red
# lip
elif [ ${task} = 'ArtVoc' ] && [[ ${cp} = "6"  ]] ; then
    colour=fsl_green
# vowel
elif [ ${task} = 'ArtVoc' ] && [[ ${cp} = "7" ]] ; then
    colour=fsl_blue

# vocalization
elif [ ${task} = 'Factorial' ] && [ ${cp} = "8" ] && [ ${ROI} = 'dorsal' ]; then
    colour=fsl_blue
elif [ ${task} = 'Factorial' ] && [ ${cp} = "8" ] && [ ${ROI} = 'ventral' ]; then
    colour=blue-lightblue

# articulation
elif [ ${task} = 'Factorial' ] && [ ${cp} = "9" ] ; then
    colour=fsl_red

# hand
elif [ ${task} = 'Judgement' ] && [ ${cp} = "7" ] ; then
    colour=fsl_yellow
fi

if [[ ! ${subj} == 'group' ]] ; then
    if [[ ! ${task} == 'all' ]] ; then
        echo "do ${subj} ${task} ${cp} ${ROI} $hemi"
        # take the individuals z-stats image from the first-level FEAT analysis as input
        featdir=$OD/func/task/feats/${task}.feat
        input=$featdir/stats/zstat${cp}.nii.gz
        echo "input file: $input"

        # ------
        # define which ROI to use and warp to functional space
        # ------
        if [[  ${ROI} == 'tongue' || ${ROI} == 'lip' || ${ROI} == 'hand' || ${ROI} == 'art' ]]; then
            # warp FS label to func-space
            applywarp -i $OD/ROIs/volumetric/ctx_S_central_${hemi}.nii.gz -r $featdir/example_func.nii.gz -o $OD/ROIs/volumetric/${task}_ctx_S_central_${hemi}_2.4mm.nii.gz --premat=${featdir}/reg/acpc_to_example_func.mat --interp=trilinear
            my_mask=$OD/ROIs/volumetric/${task}_ctx_S_central_${hemi}_2.4mm.nii.gz

        elif [[ ${ROI} == 'dorsal' ]] ; then
            echo "warp group-level dorsal mask to subject's native func space (delimited in z-dimension)"
            applywarp -i $GD/ROIs/dorsal_mask_${hemi}_2.4mm -r $featdir/example_func.nii.gz  -w ${featdir}/reg/standard2example_func_warp.nii.gz -o $OD/ROIs/drawn/peaks_anat/${task}_dorsal_mask_${hemi} --interp=trilinear

            echo "mask central sulcus"
            applywarp -i $OD/ROIs/volumetric/ctx_S_central_${hemi}.nii.gz -r $featdir/example_func.nii.gz -o $OD/ROIs/volumetric/${task}_ctx_S_central_${hemi}_2.4mm.nii.gz --premat=${featdir}/reg/acpc_to_example_func.mat --interp=trilinear
            fslmaths $OD/ROIs/volumetric/${task}_ctx_S_central_${hemi}_2.4mm.nii.gz -mas $OD/ROIs/drawn/peaks_anat/${task}_dorsal_mask_${hemi} $OD/ROIs/drawn/peaks_anat/${task}_central_dorsal_mask_${hemi}
            fslmaths $OD/ROIs/drawn/peaks_anat/${task}_central_dorsal_mask_${hemi} -bin $OD/ROIs/drawn/peaks_anat/${task}_central_dorsal_mask_${hemi}
            my_mask=$OD/ROIs/drawn/peaks_anat/${task}_central_dorsal_mask_${hemi}

        elif [[ ${ROI} == 'ventral' ]] ; then
            echo "warp manually drawn ventral ROI to funcspace"
            applywarp -i $OD/ROIs/drawn/peaks_anat/new_ventral_mask_${hemi}.nii.gz  -r $featdir/example_func.nii.gz --premat=${featdir}/reg/acpc_to_example_func.mat -o $OD/ROIs/drawn/peaks_anat/${task}_new_ventral_mask_${hemi}_2.4mm.nii.gz
            my_mask=$OD/ROIs/drawn/peaks_anat/${task}_new_ventral_mask_${hemi}_2.4mm.nii.gz

        # exclude tongue sphere for vocalization contrast
        if [[ ( ${task} == 'Factorial'  &&  ${cp} == '8' ) ]]; then
            echo "make sphere at individual's tongue peak"
            fslmaths $OD/ROIs/drawn/peaks_anat/ArtVoc_cp5_tongue_${hemi}_max.nii.gz -kernel sphere $tongue_kernel -fmean $OD/ROIs/drawn/peaks_anat/ArtVoc_cp5_tongue_${hemi}_mask.nii.gz -odt float
            fslmaths $OD/ROIs/drawn/peaks_anat/ArtVoc_cp5_tongue_${hemi}_mask.nii.gz -thr 0.00000010 -bin $OD/ROIs/drawn/peaks_anat/ArtVoc_cp5_tongue_${hemi}_mask.nii.gz

            echo "mask out tongue sphere"
            fslmaths $my_mask -sub $OD/ROIs/drawn/peaks_anat/ArtVoc_cp5_tongue_${hemi}_mask.nii.gz $my_mask
        fi

        echo "get coordinates of maximal voxel within ROI"
        new_max=`fslstats $input -k $my_mask -x`
        new_max=($new_max)

        echo "make single voxel at maximal z-value location"
        fslmaths $input -mul 0 -add 1 -roi ${new_max[0]} 1 ${new_max[1]} 1 ${new_max[2]} 1 0 1 $OD/ROIs/drawn/peaks_anat/${task}_cp${cp}_${ROI}_${hemi}_max.nii.gz -odt float

        echo "store maximal z-value as txt file"
        max_z=`fslstats $input -k $OD/ROIs/drawn/peaks_anat/${task}_cp${cp}_${ROI}_${hemi}_max.nii.gz -R`
        max_z=`echo $max_z | awk '{print $2}'`
        echo "max: $max_z"
        >$OD/ROIs/drawn/peaks_anat/${task}_cp${cp}_${ROI}_${hemi}_max.txt
        echo $max_z >> $OD/ROIs/drawn/peaks_anat/${task}_cp${cp}_${ROI}_${hemi}_max.txt

        echo "transform maximal voxel to acpc"
        applywarp -i $OD/ROIs/drawn/peaks_anat/${task}_cp${cp}_${ROI}_${hemi}_max.nii.gz -r $OD/T1w/T1w_acpc_dc_restore_brain_2mm.nii.gz --premat=${featdir}/reg/example_func_to_acpc.mat -o $OD/ROIs/drawn/peaks_acpc/${task}_cp${cp}_${ROI}_${hemi}_max.nii.gz --interp=trilinear
        max=`fslstats $OD/ROIs/drawn/peaks_acpc/${task}_cp${cp}_${ROI}_${hemi}_max.nii.gz -x`
        max=($max)
        fslmaths $OD/T1w/T1w_acpc_dc_restore_brain_2mm.nii.gz -mul 0 -add 1 -roi ${max[0]} 1 ${max[1]} 1 ${max[2]} 1 0 1 $OD/ROIs/drawn/peaks_acpc/${task}_cp${cp}_${ROI}_${hemi}_max.nii.gz -odt float

        echo 'smooth around maximal voxel to ensure surface mapping'
        fslmaths $OD/ROIs/drawn/peaks_acpc/${task}_cp${cp}_${ROI}_${hemi}_max.nii.gz -s 1 $OD/ROIs/drawn/peaks_acpc/${task}_cp${cp}_${ROI}_${hemi}_max_S.nii.gz

        echo "map native maximum voxel to native surface (acpc)"
        wb_command -volume-to-surface-mapping $OD/ROIs/drawn/peaks_acpc/${task}_cp${cp}_${ROI}_${hemi}_max_S.nii.gz $OD/T1w/Native/sub-${subj}.${hemi}.midthickness.native.surf.gii $OD/ROIs/drawn/peaks_acpc/${task}_cp${cp}_${ROI}_${hemi}_max.func.gii \
        -ribbon-constrained $OD/T1w/Native/sub-${subj}.${hemi}.white.native.surf.gii $OD/T1w/Native/sub-${subj}.${hemi}.pial.native.surf.gii

        echo "resample peak from native to 32k_fs_LR with freesurfer registration"
        wb_command -metric-resample $OD/ROIs/drawn/peaks_acpc/${task}_cp${cp}_${ROI}_${hemi}_max.func.gii \
        $OD/MNINonLinear/Native/sub-${subj}.${hemi}.sphere.reg.reg_LR.native.surf.gii \
        $OD/MNINonLinear/fsaverage_LR32k/sub-${subj}.${hemi}.sphere.32k_fs_LR.surf.gii BARYCENTRIC \
        $OD/ROIs/drawn/peaks_MNI/${task}_cp${cp}_${ROI}_${hemi}_max.func.gii

        # make a spherical patch on both native and fs_LR mesh
        for res in acpc MNI; do
            echo "smooth maximum on surface to get peak"
            max=`wb_command -metric-stats $OD/ROIs/drawn/peaks_${res}/${task}_cp${cp}_${ROI}_${hemi}_max.func.gii  -reduce MAX`
            wb_command -metric-math "(x==$max)" $OD/ROIs/drawn/peaks_${res}/${task}_cp${cp}_${ROI}_${hemi}_max.func.gii  -var x $OD/ROIs/drawn/peaks_${res}/${task}_cp${cp}_${ROI}_${hemi}_max.func.gii
            if [ $res ==  'MNI' ]; then
                smooth_surf=$OD/MNINonLinear/fsaverage_LR32k/sub-${subj}.${hemi}.midthickness.32k_fs_LR.surf.gii
            elif [ $res ==  'acpc' ]; then
                smooth_surf=$OD/MNINonLinear/Native/sub-${subj}.${hemi}.midthickness.native.surf.gii
            fi

            wb_command -metric-smoothing $smooth_surf $OD/ROIs/drawn/peaks_${res}/${task}_cp${cp}_${ROI}_${hemi}_max.func.gii 1 $OD/ROIs/drawn/peaks_${res}/${task}_cp${cp}_${ROI}_${hemi}_peak.func.gii
            # binarize peak
            wb_command -metric-math "(x>0)" $OD/ROIs/drawn/peaks_${res}/${task}_cp${cp}_${ROI}_${hemi}_peak.func.gii  -var x $OD/ROIs/drawn/peaks_${res}/${task}_cp${cp}_${ROI}_${hemi}_peak.func.gii

            echo "colour settings"
            for type in max peak; do
                wb_command -metric-palette $OD/ROIs/drawn/peaks_${res}/${task}_cp${cp}_${ROI}_${hemi}_${type}.func.gii MODE_AUTO_SCALE_PERCENTAGE -palette-name $colour
                wb_command -set-map-names $OD/ROIs/drawn/peaks_${res}/${task}_cp${cp}_${ROI}_${hemi}_${type}.func.gii -map 1 ${task}'_cp'${cp}'_'${ROI}'_'${hemi}'sub-'${subj}
            done
        done

        echo "done" $OD/ROIs/drawn/peaks_acpc/${task}_cp${cp}_${ROI}_${hemi}_max.func.gii

    elif [[ ${task} == 'all' ]]; then
        for res in acpc MNI; do
            echo 'merge all peaks of individual in one file'
            cmd_str=""
            peak_list='Judgement_cp7_hand Factorial_cp8_dorsal ArtVoc_cp5_tongue ArtVoc_cp6_lip Factorial_cp8_ventral'
            for peak in $peak_list; do
                echo "add peak " $peak
                cmd_str="$cmd_str -metric $OD/ROIs/drawn/peaks_${res}/${peak}_${hemi}_peak.func.gii"
            done
            wb_command -metric-merge $OD/ROIs/drawn/peaks_${res}/peaks_${hemi}.func.gii $cmd_str
        done
    fi

# group-level files for peaks
elif [[ ${subj} == 'group' ]] ; then
    for res in MNI; do
        echo 'merge all individual peaks to a multiple-map file and a file of added peaks'
        peaks_added=$GD/ROIs/drawn/peaks_${res}/${task}_cp${cp}_${ROI}_${hemi}_peaks_added.func.gii
        peaks_individuals=$GD/ROIs/drawn/peaks_${res}/${task}_cp${cp}_${ROI}_${hemi}_peak_individuals.func.gii
        echo "Working on ${peaks_added}"

        cmd_str=""
        for subj in $subj_list; do
            OD=$rootdir/derivatives/sub-$subj
            cmd_str="$cmd_str -metric $OD/ROIs/drawn/peaks_${res}/${task}_cp${cp}_${ROI}_${hemi}_peak.func.gii"
        done

        wb_command -metric-merge $peaks_individuals $cmd_str
        wb_command -metric-reduce $peaks_individuals SUM $peaks_added
        wb_command -metric-palette $peaks_added MODE_AUTO_SCALE_PERCENTAGE -palette-name $colour
        wb_command -set-structure $peaks_individuals $structure
        wb_command -set-structure $peaks_added $structure
        echo 'done' $peaks_added
    done

fi # if individual or group
