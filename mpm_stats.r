''' statistical evaluation of the cortical measures.'
library(readr)
library(lmerTest)
library(emmeans)

rootdir='/myDir/project_larynx'

# z-scores of mpms 
dfz_mpm_no_hand <- read_csv(c(rootdir, "/derivatives/group/dfz_mpm_no_hand.csv"))
# shapiro test for normality
shapiro.test(dfz_mpm_no_hand$value)

my_model_fit <- lmer(value ~ ROI * hemi * map + (1|subj), dfz_mpm_no_hand)
# display results of linear regression
summary(my_model_fit)
# main and interaction effects
anova(my_model_fit)
emmeans(my_model_fit, list(pairwise ~ ROI), adjust = "tukey")
emmeans(my_model_fit, list(pairwise ~ hemi), adjust = "tukey")
emmeans(my_model_fit, list(pairwise ~ hemi*ROI), adjust = "tukey")

# compare thickness values (thickness > 2)
df_thick_no_hand_gr_2 <- read_csv(c(rootdir, "/derivatives/group/df_thick_no_hand_gr_2.csv"))
my_model_fit <- lmer(value ~ ROI * hemi + (1|subj), df_thick_no_hand_gr_2)
# display results of linear regression
summary(my_model_fit)
# main and interaction effects
anova(my_model_fit)
emmeans(my_model_fit, list(pairwise ~ ROI), adjust = "tukey")


# z-scores of mpms (cleaned, i.e. sensory peaks excluded)
dfz_mpm_clean_no_hand <- read_csv(c(rootdir, "/derivatives/group/dfz_mpm_clean_no_hand.csv"))
my_model_fit <- lmer(value ~ ROI * hemi * map + (1|subj), dfz_mpm_clean_no_hand)
# display results of linear regression
summary(my_model_fit)
# main and interaction effects
anova(my_model_fit)
emmeans(my_model_fit, list(pairwise ~ ROI), adjust = "tukey")
emmeans(my_model_fit, list(pairwise ~ hemi), adjust = "tukey")

