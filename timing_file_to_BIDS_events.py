'''Convert the information in a FEAT timing file for block design to a BIDS events file.'''

import os
import sys
import pandas as pd

# ----------
# overhead
# ----------
rootdir = '/myDir/project_larynx'

# get list of subjects
subs = pd.read_csv(os.path.join(rootdir, 'participants.tsv'), sep='\t', dtype={'id': str})
task_list=['ArtVoc', 'Factorial', 'Judgement']

length_tasks = {'ArtVoc': 480, \
'Factorial': 600, \
'Judgement': 528
}

conditions_ArtVoc = {'ev1': 'tongue', \
'ev2': 'lip', \
'ev3': 'vowel', \
'ev4': 'breathe', \
'no_ev': 'rest'}

conditions_Factorial = {'ev1': 'overt speech', \
'ev2': 'mouthing', \
'ev3': 'vowel', \
'ev4': 'covert speech', \
'no_ev': 'rest'}

conditions_Judgement = {'ev1': 'phonological', \
'ev2': 'semantic', \
'ev3': 'visual', \
'no_ev': 'rest'}

# loop over subjects
for ind, sub_row in subs.iterrows():
    print(sub_row.id)
    # directory where rawdata is stored. Output directory for events.tsv file
    DD = os.path.join(myscratch, 'LarynxRepresentation', 'sub-' + sub_row.id)
    # directory where feat folders are stored
    OD = os.path.join(myscratch, 'LarynxRepresentation', 'derivatives', 'sub-' + sub_row.id)

    # loop over tasks
    for task in task_list:
        print(task)
        if task == 'ArtVoc':
            con_dict = conditions_ArtVoc
        elif task == 'Factorial':
            con_dict = conditions_Factorial
        elif task == 'Judgement':
            con_dict = conditions_Judgement

        events_df = pd.DataFrame(columns=('onset', 'duration', 'condition'))

        # loop over task EVs
        for ev, condition in con_dict.items():
            if ev != 'no_ev':
                # derive timing file for the EV and extract values for block onset and duration
                timing_file = os.path.join(OD, 'func', 'task', 'feats', task + '.feat', 'custom_timing_files', ev + '.txt')
                with open(timing_file) as f:
                    f_txt = f.readlines()
                    for line in f_txt:
                        block = str.split(line.rstrip())
                        events_df = events_df.append({'onset':int(block[0]), 'duration': int(block[1]), 'condition': condition}, ignore_index=True)

        events_df = events_df.sort_values(by='onset')
        events_df = events_df.reset_index(drop=True)
        max_row = pd.to_numeric(events_df.onset).idxmax()

        # search for undefined time segments and fill with baseline condition
        for ind, row in events_df.iterrows():
            new_row_onset = row.onset + row.duration
            if ind < max_row :
                new_row_duration = events_df['onset'][ind + 1] - new_row_onset
            else:
                new_row_duration = length_tasks[task] - new_row_onset
            events_df = events_df.append({'onset': new_row_onset, 'duration': new_row_duration, 'condition': con_dict['no_ev']}, ignore_index=True)

        events_df = events_df.sort_values(by='onset')
        events_df = events_df.reset_index(drop=True)

        # save output
        out_file = os.path.join(DD, 'func', 'sub-' + sub_row.id + '_task-' + task + '_events.tsv')
        events_df.to_csv(out_file, sep='\t', index=False)
