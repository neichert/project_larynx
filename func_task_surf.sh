''' Map individual activations to surface and compute count maps to derive individual subject maps (Figure 2B, 3B):
bash func_task_surf.sh $subj $task $cp --individual
Create group count maps from individual surface maps:
bash func_task_surf.sh group $task $cp --group'''

# parse options
getoption() {
  sopt="--$1"
  shift 1
  for fn in $@ ; do
  	if [[ -n $(echo $fn | grep -- "^${sopt}=") ]] ; then
      echo $fn | sed "s/^${sopt}=//"
      return 0
    elif [[ -n $(echo $fn | grep -- "^${sopt}$") ]] ; then
      echo "TRUE" # if not string is given, set it the value of the option to TRUE
      return 0
  	fi
  done
}

# Defaults
rootdir='/myDir/project_larynx'

subj=$1
task=$2
cp=$3
OD=$rootdir/derivatives/sub-$subj
GD=$rootdir/derivatives/group
p_val=0.05
colour=FSL
subj_list='01 02 03 04 05 06 07 08 09 11 12 13 14 15 16 17 18 19 20 21'
display_thr=3.1 # just for visual assessment, not for statistics
hemi_list='L R'


##############
## Task settings
##############
  case $task in
  'ArtVoc' )
      gfeatname=ArtVoc ;;
  'Factorial' )
      gfeatname=Factorial ;;
  'Judgement' )
      gfeatname=Judgement ;;
  esac

  ##############
  ## Cope settings
  ##############
  # tongue
  if [ $task = 'ArtVoc' ] && [[ $cp = "5" || $cp = "8" ]] ; then
    colour=fsl_red
  # lip
  elif [ $task = 'ArtVoc' ] && [[ $cp = "6" || $cp = "9" ]] ; then
      ROI_list='lip'
  # vowel
  elif [ $task = 'ArtVoc' ] && [[ $cp = "7" || $cp = "10" ]] ; then
      colour=fsl_blue

  # vocalization
  elif [ $task = 'Factorial' ] && [ $cp = "8" ] ; then
      colour=fsl_blue
  # articulation
  elif [ $task = 'Factorial' ] && [ $cp = "9" ] ; then
      colour=fsl_red
  # hand
  elif [ $task = 'Judgement' ] && [ $cp = "7" ] ; then
      colour=yellow
  fi

  ##############
  ## Loop for individuals
  ##############
  if [[ $(getoption "individual" "$@") = "TRUE" ]] ; then
      echo "Working on $task cope $cp subj-$subj"
      featdir=$OD/func/task/feats/${task}.feat

      echo "downsamle T1w reference to 2.4mm"
      flirt -dof 6 -cost corratio -nosearch -in $OD/T1w/T1w_acpc_dc_restore_brain.nii.gz -ref $OD/T1w/T1w_acpc_dc_restore_brain.nii.gz -omat $OD/T1w/acpc_to_2mm.mat -applyisoxfm 2.4 -out $OD/T1w/T1w_acpc_dc_restore_brain_2.4mm.nii.gz
      applywarp --rel -i $OD/T1w/T1w_acpc_dc_restore_brain.nii.gz -r $OD/T1w/T1w_acpc_dc_restore_brain_2.4mm.nii.gz -o $OD/T1w/T1w_acpc_dc_restore_brain_2.4mm.nii.gz --premat=$OD/T1w/acpc_to_2mm.mat --interp=spline

      # get linear transformation from functional space to FreeSurfers acpc space
      echo "run flirt for func2acpc"
      flirt -dof 6 -cost corratio -nosearch -in $featdir/example_func.nii.gz -ref $OD/T1w/T1w_acpc_dc_restore_brain.nii.gz -omat $featdir/reg/example_func2acpc.mat

      echo "warp zstats to acpc"
      applywarp --rel -i $featdir/stats/zstat${cp} -r $OD/T1w/T1w_acpc_dc_restore_brain_2.4mm.nii.gz -o $featdir/stats/zstat${cp}.acpc --premat=$featdir/reg/example_func2acpc.mat --interp=trilinear

      # determine individual voxel-wise threshold for the scan
      smoothness=`cat $featdir/stats/smoothness`
      vol=`echo $smoothness | cut -d " " -f 4`
      resels=`echo $smoothness | cut -d " " -f 6`
      n_resels=`echo "$vol/$resels" | bc -l`
      thr=`ptoz $p_val -g $n_resels`
      if [ $task = 'ArtVoc' ] && [[ $cp = "4" ]]; then
          thr=1.96
          echo "set thr=1.96 for ArtVoc cope 4 breathing"
      fi

      echo "individual voxel-wise threshold: " $thr
      > $featdir/stats/individual_thres.txt
      echo $thr >> $featdir/stats/individual_thres.txt

      ##############
      ## Map to surface
      ##############
      for hemi in $hemi_list; do
          echo "do ${hemi}H"

          echo "map zstats to native surface"
          wb_command -volume-to-surface-mapping $featdir/stats/zstat${cp}.acpc.nii.gz \
          $OD/T1w/Native/sub-${subj}.${hemi}.midthickness.native.surf.gii $featdir/stats/zstat${cp}_${hemi}.native.func.gii \
          -ribbon-constrained $OD/T1w/Native/sub-${subj}.${hemi}.white.native.surf.gii \
          $OD/T1w/Native/sub-${subj}.${hemi}.pial.native.surf.gii

          echo "resample zstats from native to standard 32k_fs_LR mesh"
          wb_command -metric-resample $featdir/stats/zstat${cp}_${hemi}.native.func.gii \
          $OD/MNINonLinear/Native/sub-${subj}.${hemi}.sphere.reg.reg_LR.native.surf.gii \
          $OD/MNINonLinear/fsaverage_LR32k/sub-${subj}.${hemi}.sphere.32k_fs_LR.surf.gii \
          ADAP_BARY_AREA \
          $featdir/stats/zstat${cp}_${hemi}.32k_fs_LR.func.gii \
          -area-surfs $OD/T1w/Native/sub-${subj}.${hemi}.midthickness.native.surf.gii \
          $OD/MNINonLinear/fsaverage_LR32k/sub-${subj}.${hemi}.midthickness.32k_fs_LR.surf.gii \
          -current-roi $OD/MNINonLinear/Native/sub-${subj}.${hemi}.roi.native.shape.gii

          echo "apply voxelwise threshold to zstat on standard mesh"
          wb_command -metric-math "(x>$thr)" $featdir/stats/zstat${cp}_${hemi}.32k_fs_LR_bin.func.gii -var x $featdir/stats/zstat${cp}_${hemi}.32k_fs_LR.func.gii
          wb_command -metric-mask $featdir/stats/zstat${cp}_${hemi}.32k_fs_LR.func.gii $featdir/stats/zstat${cp}_${hemi}.32k_fs_LR_bin.func.gii $featdir/stats/zstat${cp}_${hemi}.32k_fs_LR_t.func.gii
          echo "done $featdir/stats/zstat${cp}_${hemi}.32k_fs_LR_t.func.gii"
      done # for hemi

  ##############
  ## Group count maps
  ##############
  elif [[ $(getoption "group" "$@") = "TRUE" ]] ; then
      for hemi in $hemi_list; do
          echo "Working on $task cope $cp ${hemi}H"
          individual_maps=$GD/func/task/feats/${gfeatname}_zstats_cp${cp}_${hemi}_individuals.func.gii
          count_map=$GD/func/task/feats/${gfeatname}_zstats_cp${cp}_${hemi}.func.gii
          cmd_str_individuals=""
          cmd_str_count=""
          for subj in $subj_list; do
              #echo "Working on subj-$subj"
              OD=~/scratch/LarynxRepresentation/derivatives/sub-$subj
              featdir=$OD/func/task/feats/${task}.feat
              cmd_str_individuals="$cmd_str_individuals -metric $featdir/stats/zstat${cp}_${hemi}.32k_fs_LR.func.gii"
              cmd_str_count="$cmd_str_count -metric $featdir/stats/zstat${cp}_${hemi}.32k_fs_LR_bin.func.gii"
          done
          wb_command -metric-merge $individual_maps $cmd_str_individuals
          wb_command -metric-merge $count_map $cmd_str_count
          wb_command -metric-reduce $count_map SUM $count_map

          echo "colour settings"
          wb_command -metric-palette $individual_maps MODE_AUTO_SCALE_PERCENTAGE -palette-name $colour -thresholding THRESHOLD_TYPE_NORMAL THRESHOLD_TEST_SHOW_OUTSIDE 0 $display_thr -disp-neg false
          wb_command -metric-palette $count_map MODE_USER_SCALE -pos-user 4 20 -palette-name JET256 -thresholding THRESHOLD_TYPE_NORMAL THRESHOLD_TEST_SHOW_OUTSIDE 0 4 -disp-neg false
      done # for hemi
  fi # if individual or group
