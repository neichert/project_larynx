'''Process MPM maps that had been derived from the hMRI toolbox.
run first for all subjects:   fsl_sub bash MPM_process_maps.sh $subj --pre
Then check registration quality.
Then run:   fsl_sub bash MPM_process_maps.sh $subj $map $hemi --surf
To create average map:
bash process_MPM.sh --group $map $hemi'''

# parse option
getoption() {
  sopt="--$1"
  shift 1
  for fn in $@ ; do
  	if [[ -n $(echo $fn | grep -- "^${sopt}=") ]] ; then
      echo $fn | sed "s/^${sopt}=//"
      return 0
    elif [[ -n $(echo $fn | grep -- "^${sopt}$") ]] ; then
      echo "TRUE" # if not string is given, set it the value of the option to TRUE
      return 0
  	fi
  done
}

rootdir='/myDir/project_larynx'

subj=$1
map=$2
hemi=$3
echo "do sub-"$subj
DD=$rootdir/derivatives/sub-$subj/MPM/PSN_results/Results
OD=$rootdir/derivatives/sub-$subj
subj_list='01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20'
GD=$rootdir/derivatives/group

## --------------------------
## Brain extraction and registration
## --------------------------
if [[ $(getoption "pre" "$@") = "TRUE" ]] ; then

    maptype="R1 R2s_OLS MT"
    tmpdir=$(mktemp -d "/tmp/mpm.XXXXXXXXXX")
    # use MT image to get field-of-view. Crop image:
    #robustfov -i $DD/s*_MT.nii -r $tmpdir/MT_crop
    # Final FOV is:
    fov='0 224 56 170 0 176'
    fslroi $DD/s*_MT.nii $tmpdir/MT_crop $fov

    # bain-extract MT image and get brainmask
    bet $tmpdir/MT_crop $OD/MPM/MT_brain -f 0.3 -m
    # maybe erode the brain mask if you want you need it to be tighter
    # fslmaths $OD/MPM/MT_brain_mask -ero $OD/MPM/MT_brain_mask
    # or dilate
    # fslmaths $OD/MPM/MT_brain_mask -dilM $OD/MPM/MT_brain_mask
    brain_mask=$OD/MPM/MT_brain_mask

    # loop over MPM maps and apply the brain mask
    for map in $maptype; do
        echo "do " $map
        # crop maps to the FOV derived from PDw
        fslroi $DD/s*_${map}.nii $tmpdir/${map}_crop $fov
        # apply brain mask
        fslmaths $tmpdir/${map}_crop -mas $brain_mask $OD/MPM/${map}_brain
        fslmaths $OD/MPM/${map}_brain -thr 0 $OD/MPM/${map}_brain
        # reorient
        fslreorient2std $OD/MPM/${map}_brain $OD/MPM/${map}_brain_re
    done

    echo "register with MPMs with FreeSurfer's native acpc and standard space"
    # get transformation to acpc
    flirt -dof 12 -cost corratio -nosearch -in $OD/MPM/MT_brain_re.nii.gz -ref $OD/T1w/T1w_acpc_dc_restore_brain.nii.gz -omat $OD/MPM/maps2acpc.mat -out $OD/MPM/MT_acpc
    # fsleyes $OD/T1w/T1w_acpc_dc_restore_brain.nii.gz $OD/MPM/MT_acpc

    # transform to MNI
    applywarp -i $OD/MPM/MT_acpc.nii.gz -r $OD/MNINonLinear/T1w_restore.nii.gz -o $OD/MPM/MT_mni --warp=$OD/MNINonLinear/xfms/acpc_dc2standard.nii.gz --interp=spline
    #fsleyes $OD/MNINonLinear/T1w_restore_brain.nii.gz $OD/MPM/MT_mni

    # combine warps
    fsl_sub -q veryshort.q convertwarp --ref=$OD/MNINonLinear/T1w_restore.nii.gz --premat=$OD/MPM/maps2acpc.mat --warp1=$OD/MNINonLinear/xfms/acpc_dc2standard.nii.gz --out=$OD/MPM/maps2mni_warp

    echo "warp maps to acpc and mni space"
    for map in $maptype; do
      echo "do " $map
        applywarp -i $OD/MPM/${map}_brain_re.nii.gz -r $OD/T1w/T1w_acpc_dc_restore_brain.nii.gz -o $OD/MPM/${map}_acpc --premat=$OD/MPM/maps2acpc.mat --interp=spline
        applywarp -i $OD/MPM/${map}_brain_re.nii.gz -r $OD/MNINonLinear/T1w_restore.nii.gz -o $OD/MPM/${map}_mni --warp=$OD/MPM/maps2mni_warp --interp=spline
    done

    # copy T1vsT2 to MPM folder
    cp $OD/T1w/T1wDividedByT2w.nii.gz $OD/MPM/T1vsT2_acpc.nii.gz
    applywarp -i $OD/T1w/T1wDividedByT2w.nii.gz -r $OD/MNINonLinear/T1w_restore.nii.gz -o $OD/MPM/T1vsT2_mni --warp=$OD/MNINonLinear/xfms/acpc_dc2standard.nii.gz

    # copy thickness surface map to MPM folder
    for hemi in L R; do
        cp $OD/MNINonLinear/Native/sub-${subj}.${hemi}.thickness.native.shape.gii $OD/MPM/thickness.${hemi}.func.gii
    done
fi

## --------------------------
## Surface Mapping
## --------------------------
if [[ $(getoption "surf" "$@") = "TRUE" ]] ; then
    # get these parameters from the HCP processing pipeline
    MyelinMappingFWHM="5"
    SurfaceSmoothingFWHM="4"
    MyelinMappingSigma=`echo "$MyelinMappingFWHM / ( 2 * ( sqrt ( 2 * l ( 2 ) ) ) )" | bc -l`
    SurfaceSmoothingSigma=`echo "$SurfaceSmoothingFWHM / ( 2 * ( sqrt ( 2 * l ( 2 ) ) ) )" | bc -l`

    if [[ ! $map == 'all' ]] ; then
        echo "map" $map $hemi"H to surface"
        # myelin method
        wb_command -volume-to-surface-mapping $OD/MPM/${map}_acpc.nii.gz $OD/T1w/Native/sub-${subj}.${hemi}.midthickness.native.surf.gii $OD/MPM/${map}.${hemi}.func.gii \
        -myelin-style $OD/T1w/sub-${subj}/mri/ribbon.postT2.pass1/${hemi,,}h.ribbon.nii.gz $OD/MNINonLinear/Native/sub-${subj}.${hemi}.corrThickness.native.shape.gii $MyelinMappingSigma

        echo "smoothing"
        wb_command -metric-smoothing $OD/T1w/Native/sub-${subj}.${hemi}.white.native.surf.gii $OD/MPM/${map}.${hemi}.func.gii $SurfaceSmoothingSigma $OD/MPM/${map}.${hemi}_S.func.gii -fix-zeros

        echo "resample smoothed maps from native to standard mesh"
        wb_command -metric-resample $OD/MPM/${map}.${hemi}_S.func.gii \
        $OD/MNINonLinear/Native/sub-${subj}.${hemi}.sphere.reg.reg_LR.native.surf.gii \
        $OD/MNINonLinear/sub-${subj}.${hemi}.sphere.164k_fs_LR.surf.gii \
        ADAP_BARY_AREA \
        $OD/MPM/${map}.${hemi}.164k_fs_LR_S.func.gii \
        -area-surfs $OD/T1w/Native/sub-${subj}.${hemi}.midthickness.native.surf.gii \
        $OD/MNINonLinear/sub-${subj}.${hemi}.midthickness.164k_fs_LR.surf.gii \
        -current-roi $OD/MNINonLinear/Native/sub-${subj}.${hemi}.roi.native.shape.gii

        wb_command -set-map-names $OD/MPM/${map}.${hemi}_S.func.gii -map 1 ${map}'_sub-'$subj
        wb_command -set-map-names $OD/MPM/${map}.${hemi}.164k_fs_LR_S.func.gii -map 1 ${map}'_sub-'$subj

    elif [[ $map == 'all' ]] ; then
        echo "combine all measures"
        cmd_str=''
        for map in MT R1 R2s_OLS T1vsT2 thickness; do
            cmd_str="$cmd_str -metric $OD/MPM/${map}.${hemi}_S.func.gii"
        done
        wb_command -metric-merge $OD/MPM/all_maps_${hemi}.func.gii $cmd_str
    fi

elif [[ $(getoption "group" "$@") = "TRUE" ]] ; then
    if [[ ! $map == 'all' ]] ; then
        echo "get average map:" $map
        individuals_map=$GD/MPM/${map}.${hemi}.164k_fs_LR_individuals.func.gii
        mean_map=$GD/MPM/${map}.${hemi}_mean.func.gii
        cmd_str=''
        for subj in $subj_list; do
            OD=~/scratch/LarynxRepresentation/derivatives/sub-$subj
            if [[ $map = 'MyelinMap' || $map = 'SmoothedMyelinMap' || $map = 'SmoothedMyelinMap_BC' ]]; then
                ind_map=$OD/MNINonLinear/sub-${subj}.${hemi}.${map}.164k_fs_LR.func.gii
            else
                ind_map=$OD/MPM/${map}.${hemi}.164k_fs_LR_S.func.gii
            fi
            cmd_str="$cmd_str -metric $ind_map"
        done

        wb_command -metric-merge $individuals_map $cmd_str
        wb_command -metric-reduce $individuals_map MEAN $mean_map
        wb_command -metric-palette $individuals_map MODE_AUTO_SCALE_PERCENTAGE -palette-name videen_style -pos-percent 4 96
        wb_command -metric-palette $mean_map MODE_AUTO_SCALE_PERCENTAGE -palette-name videen_style -pos-percent 4 96
    fi
fi