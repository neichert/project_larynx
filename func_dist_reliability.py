'''quantify reliability of peaks between tasks in individuals'''

import numpy as np
import os
import sys
import pdb
import subprocess
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import nibabel as nib

rootdir = '/myDir/project_larynx'

GD = os.path.join(rootdir, 'derivatives', 'group')
distances_fname = os.path.join(GD, 'func', 'reliability.pkl')
tmpfilename = os.path.join(GD, 'func', 'reliability.func.gii')

colour_list = np.array([[0.,   0.6,  1.  ],
                         [0.,   0.7,  0.  ],
                         [0.,   0.63, 0.68],
                         [0.,   0.67, 0.34],
                         [0.68, 0.4,  1.  ],
                         [0.68, 0.5,  0.  ],
                         [1.,   0.33, 0.68],
                         [1.,   0.37, 0.34]])

# which pairs to compare
df_pairs = pd.DataFrame(columns=["Factorial_peak", "ArtVoc_peak", "short_name", "colour"])
df_pairs.loc[len(df_pairs)] = ['Factorial_cp8_dorsal', 'ArtVoc_cp7_dorsal', 'dorsal', np.array([0, 0, 1])]
df_pairs.loc[len(df_pairs)] = ['Factorial_cp9_art', 'ArtVoc_cp5_tongue', 'tongue', colour_list[7]]
df_pairs.loc[len(df_pairs)] = ['Factorial_cp8_ventral', 'ArtVoc_cp7_ventral', 'ventral', colour_list[0]]

subj_list = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '11', '10', '12', '13', '14', '15', '16', '17', '18', '19', '20']
hemi_list = ["L", "R"]

df = pd.DataFrame(columns=["subj", "ROI", "hemi", 'dist'])
my_row = 0
for subj in subj_list:
    for hemi in hemi_list:
        for ind, pair in df_pairs.iterrows():
            print(subj, hemi, pair.short_name)
            OD = os.path.join(myscratch, 'LarynxRepresentation', 'derivatives', f'sub-{subj}')
            surface = os.path.join(OD, 'T1w', 'Native', f'sub-{subj}.{hemi}.midthickness.native.surf.gii')
            # vertex of first peak
            peak1 = os.path.join(OD, 'ROIs', 'drawn', f'peaks_acpc', f'{pair.Factorial_peak}_{hemi}_peak.func.gii')
            # vertex of second peak
            vx_peak1 = np.argmax(nib.load(peak1).darrays[0].data)
            peak2 = os.path.join(OD, 'ROIs', 'drawn', f'peaks_acpc', f'{pair.ArtVoc_peak}_{hemi}_peak.func.gii')
            # derive distance
            os.system("wb_command -surface-geodesic-distance " + surface + " " + str(vx_peak1) + " " + tmpfilename)
            p = subprocess.Popen("wb_command -metric-stats " + tmpfilename + " -reduce MEAN -roi " + peak2, stdout=subprocess.PIPE, shell=True)
            dist = np.float(p.communicate()[0])
            # add to dataframe
            df.loc[my_row] = [subj, pair.short_name, hemi, dist]
            my_row = my_row + 1

# plot result
sns.catplot(x="ROI", y="dist", data=df, dodge=True, col='hemi', height=2, aspect=3)
plt.show()
# summary statistics
df.groupby(['ROI', 'hemi']).median().reset_index()
df.groupby(['ROI', 'hemi']).max().reset_index()
df.groupby(['ROI', 'hemi']).min().reset_index()

print("save: ", distances_fname)
df.to_pickle(distances_fname)
os.remove(tmpfilename)
