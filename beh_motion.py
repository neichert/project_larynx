'''quantify motion outliers to check that they are not exceding the limits'''

import numpy as np
import os
import sys
import matplotlib.pyplot as plt
import pdb

rootdir = '/myDir/project_larynx'

subs = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20']
for task in ["ArtVoc", "Factorial", "Judgement"]:
    outliers_percentages = np.array([])
    for ind, sub in enumerate(subs):
        print(sub)
        OD = os.path.join(myscratch, 'LarynxRepresentation', 'derivatives', 'sub-' + sub)
        # motion outliers
        if task == 'ArtVoc' or task == 'Factorial' or task == 'Judgement':
            outliers = open(os.path.join(OD, 'beh', 'sub-' + sub + '_' + task + '_outliers'), 'r').readlines()
            n_excluded = 0
            for volume in np.arange(len(outliers)):
                my_row = np.array(outliers[volume].rstrip().split("   ")).astype(int)
                if any(my_row==1):
                    n_excluded = n_excluded + 1

            outliers_percentages = np.append(outliers_percentages, n_excluded/np.float(len(outliers)) * 100)
            print('Outliers in ' + task + ": " + str(n_excluded) + '( ' + str(np.round(n_excluded/np.float(len(outliers)) * 100, 2)) + ' %)')

        # motion parameters
        confoundevs = open(os.path.join(OD, 'func', 'task', 'feats', task + '.feat', 'confoundevs.txt'), 'r').readlines()

        ev = 2
        trace = np.array([0])
        # relative change of the trace from the previous volume
        rel_trace = np.array([])
        for volume in np.arange(len(confoundevs)):
            my_row = np.array(confoundevs[volume].rstrip().replace("   ", "  ").split("  ")).astype(float)[ev]
            rel_trace = np.append(rel_trace, abs(my_row - trace[-1]))
            trace = np.append(trace, my_row)

        print("maximal relative movement (mm): " + str(np.round(np.max(rel_trace) * 100, 2)))
        plt.plot(rel_trace, color='red')
        plt.plot(trace)
        plt.show()

    print('range outliers percent ' + task + ' : ' + str(np.round(np.min(outliers_percentages), 2)) + ' to ' + str(np.round(np.max(outliers_percentages),2)) + " (median: " + str(np.round(np.median(outliers_percentages),2)) + ")")
