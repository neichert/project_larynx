'''derive the plots for cortical microstructure (Figure 5)'''

import numpy as np
import os
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from scipy.spatial.distance import cityblock

# ----------
# overhead
# ----------
rootdir = '/myDir/project_larynx'

colour_list = np.array([[0.,   0.6,  1.  ],
                         [0.,   0.7,  0.  ],
                         [0.,   0.63, 0.68],
                         [0.,   0.67, 0.34],
                         [0.68, 0.4,  1.  ],
                         [0.68, 0.5,  0.  ],
                         [1.,   0.33, 0.68],
                         [1.,   0.37, 0.34]])

# settings for the ROIs and the cortical maps
df_ROIs = pd.DataFrame(columns=["long_name", "short_name", "colour"])
df_ROIs.loc[len(df_ROIs)] = ['Judgement_cp7_hand', 'hand', colour_list[5]]
df_ROIs.loc[len(df_ROIs)] = ['Factorial_cp8_dorsal', 'dorsal\nlarynx', np.array([0, 0, 1])]
df_ROIs.loc[len(df_ROIs)] = ['ArtVoc_cp6_lip', 'lip', colour_list[1]]
df_ROIs.loc[len(df_ROIs)] = ['ArtVoc_cp5_tongue', 'tongue', colour_list[7]]
df_ROIs.loc[len(df_ROIs)] = ['Factorial_cp8_ventral', 'ventral\nlarynx', colour_list[0]]
df_ROIs.loc[len(df_ROIs)] = ['Factorial_cp8_precentral', 'precentral', colour_list[0]]

df_maps = pd.DataFrame(columns=["name", "colour"])
df_maps.loc[len(df_maps)] = ['T1vsT2', colour_list[5]]
df_maps.loc[len(df_maps)] = ['MT', np.array([0, 0, 1])]
df_maps.loc[len(df_maps)] = ['R1', colour_list[7]]
df_maps.loc[len(df_maps)] = ['R2s_OLS', colour_list[1]]
df_maps.loc[len(df_maps)] = ['thickness', np.array([1, 1, 1])]

subs = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20']
hemi_list = ["L", "R"]

# Load data
df = pd.DataFrame(columns=["subj", "hemi", "ROI", "map", "value", "colour_ROI", "colour_map"])
my_row = 0
for ind_s, sub in enumerate(subs):
    print(sub)
    OD = os.path.join(rootdir, 'derivatives', 'sub-' + sub)
    for ind_r, ROI in df_ROIs.iterrows():
        for hemi in hemi_list:
            for ind_m, map in df_maps.iterrows():
                # derive MPM value underlying the ROI
                myval = open(os.path.join(OD, 'MPM', ROI.long_name + '_' + hemi + '_' + map['name'] + '_max_s.txt'), 'r').readlines()[0].rstrip()
                df.loc[my_row] = [sub, hemi, ROI.short_name, map['name'], float(myval), ROI.colour, map.colour]
                my_row = my_row + 1

# df: raw values
# exclude hand in RH
df[(df.ROI == 'hand') & (df.hemi == "R")] = None
# delete R1 subj-19
df[(df.subj == '19')] = None
df.dropna(inplace=True)

# df_clean: sensory peaks excluded when below 2
df_clean = df.copy()
df_exclude = df[((df.map == 'thickness') & (df.value < 2))].groupby(['subj', 'ROI', 'hemi']).mean().reset_index()
for ind, ex in df_exclude.iterrows():
    df_clean = df_clean[~((df_clean['subj'] == ex['subj']) & (df_clean['ROI'] == ex['ROI']) & (df_clean['hemi'] == ex['hemi']))]

df_clean.dropna(inplace=True)

# convert to z-stats
# dfz: z-converted values
# dfz_clean: z-converted values without sensory peaks
for in_df in [df, df_clean]:
    out = pd.DataFrame()
    for i in list(in_df['map'].unique()):
        a = in_df[in_df.loc[:, 'map'] == i]
        my_mean = np.nanmean(a.value)
        my_std = np.nanstd(a.value)
        a.value = (a.value - my_mean) / my_std
        out = out.append(a)
        if in_df.equals(df):
            dfz = out.copy()
        elif in_df.equals(df_clean):
            dfz_clean = out.copy()

# make subsets of data
df_mpm = df[~df['map'].isin(['thickness'])]
dfz_mpm = dfz[~dfz['map'].isin(['thickness'])]
df_clean_mpm = df_clean[~df_clean['map'].isin(['thickness'])]
dfz_clean_mpm = dfz_clean[~dfz_clean['map'].isin(['thickness'])]
df_thick = df[df['map'].isin(['thickness'])]
dfz_mpm_no_hand = dfz_mpm[~dfz_mpm['ROI'].isin(['hand'])]
df_thick_no_hand = df_thick[~df_thick['ROI'].isin(['hand'])]
df_thick_no_hand_gr_2 = df_thick_no_hand[df_thick_no_hand.value > 2]
dfz_mpm_clean_no_hand = dfz_clean_mpm[~dfz_clean_mpm['ROI'].isin(['hand'])]

# write out to csv for stats in R
print('write out to csv')
df_clean.to_csv(os.path.join(rootdir, 'derivatives', 'group', 'df_clean.csv'), index=False)
dfz_mpm_no_hand.to_csv(os.path.join(rootdir, 'derivatives', 'group', 'dfz_mpm_no_hand.csv'), index=False)
df_thick_no_hand_gr_2.to_csv(os.path.join(rootdir, 'derivatives', 'group', 'df_thick_no_hand_gr_2.csv'), index=False)
dfz_mpm_clean_no_hand.to_csv(os.path.join(rootdir, 'derivatives', 'group', 'dfz_mpm_clean_no_hand.csv'), index=False)

# plot z-values for all ROIs and maps
pl1 = sns.catplot(x="ROI", y="value", data=dfz_mpm, col='hemi', hue="map", dodge=True, height=3, aspect=1, palette=sns.cubehelix_palette(4), legend=False)
plt.show()

# plot thickness
colours = [tuple(col) for col in df_thick.colour_ROI]
indexes = np.unique(colours, axis=0, return_index=True)[1]
col_list = [colours[index] for index in sorted(indexes)]
pl2 = sns.catplot(x="ROI", y="value", data=df_thick, col='hemi', hue="ROI", height=3, aspect=1, palette=sns.set_palette(col_list), legend=False)
plt.show()

# get confusion matrix of cleaned values
labels = df_ROIs.short_name.to_list()
labels.insert(0, '')

for dat in [df_clean_mpm]:
    conf_mat = np.zeros((2, 6, 6, 20))
    conf_mat.fill(np.nan)
    for i_hemi, hemi in enumerate(hemi_list):
        for i_sub, sub in enumerate(subs):
            for i_ROI1, ROI1 in df_ROIs.iterrows():
                    vals_ROI1 = dat[(dat.hemi == hemi) & (dat.subj == sub) & (dat.ROI == ROI1.short_name)].value.values
                    if vals_ROI1.size:
                        for i_ROI2, ROI2 in df_ROIs.iterrows():
                                vals_ROI2 = dat[(dat.hemi == hemi) & (dat.subj == sub) & (dat.ROI == ROI2.short_name)].value.values
                                if vals_ROI2.size:
                                    conf_mat[i_hemi, i_ROI1, i_ROI2, i_sub] = cityblock(vals_ROI1, vals_ROI2)

    # average by subject and then by hemisphere
    conf_mat_mean = np.nanmean(conf_mat, 3)
    conf_mat_mean_mean = np.nanmean(conf_mat_mean, 0)
    # plot
    plt.figure(figsize=(8, 8))
    plt.imshow(conf_mat_mean_mean, cmap='Purples')
    plt.colorbar()
    ax = plt.gca()
    ax.set_xticklabels(labels)
    ax.set_yticklabels(labels)
    ax.tick_params(axis='both', which='major', labelsize=20)
    plt.show()
