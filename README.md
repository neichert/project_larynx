This repository contains processing code for the following publication:

Eichert N, Papp D, Mars RB & Watkins KE (2020) Mapping human laryngeal motor cortex during vocalization. Cerebral Cortex (accepted). 

Processing code is stored in the subfolder /scripts. This folder also contains the file `code_instructions.md`, which contains information of how to run the code.

For further questions, please contact Nicole Eichert at nicole.eichert@psy.ox.ac.uk.
