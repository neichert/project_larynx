'''script to loop through all individual recordings and to score what was heard.
The output is stored in a csv file for each task and for each subject.'''

import os
import numpy as np
import pandas as pd
import sys
import glob
import wave
import sounddevice as sd
import time

rootdir = '/myPath/project_larynx'
task = 'ArtVoc' # or 'factorial'

# define subject IDs
subj_list = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20']
subs = pd.read_csv(os.path.join(rootdir, 'participants.tsv'), sep='\t', dtype={'id': str})
subs = subs[subs['id'].isin(subj_list)]

if task == 'ArtVoc':
    n_blocks = 32 # task block + rest blocks
elif task == 'factorial':
    n_blocks = 40 # 4 conditions, each followed by a rest blocks, 5 repetitions

# sampling rate of recording
fs = 22050
sd.default.samplerate = fs

for ind, sub_row in subs.iterrows():
    print( "do subject " + str(sub_row.id) + ', task: ' + task)
    folder = os.path.join(rootdir, 'sub-' + sub_row.id, 'beh', 'data_ArtVoc', 'sub-' + sub_row.id.lstrip("0"),
                                'recordings_' + task)
    outfile = os.path.join(rootdir, 'derivatives',
                                'sub-' + sub_row.id, 'beh',
                                task + '_rec_types.csv')
    print(outfile)
    if not os.path.isfile(outfile):
        print('initialize new dataframe with default values...')
        num_block = np.arange(1, n_blocks + 1, dtype=int)[:, np.newaxis]
        cond_list = np.zeros((n_blocks, 1), dtype=int)
        expected = np.zeros((n_blocks, 1), dtype=int)
        rec_types = np.zeros((n_blocks, 1))
        rec_types.fill(99)
        start_block = 0

        # loop over each task block
        for n_block in np.arange(start_block, n_blocks):
            # get the file name of the recording
            rec = glob.glob(folder + '/' + sub_row.id.lstrip("0") + '_' + str(n_block + 1) + '_*')[0]
            fname = os.path.splitext(os.path.basename(rec))[0]
            # derive condition and block number from filename
            condition = fname.split("_")[2]
            count_big_block = fname.split("_")[3]
            cond_list[n_block] = condition

            # code for the sounds:
            # silence: 0
            # vowel ee: 1
            # la-le-li: 2
            if task == 'ArtVoc':
                if condition == '1':
                    # silence
                    expected[n_block] = 0
                elif condition == '2':
                    # silence
                    expected[n_block] = 0
                elif condition == '3':
                    # vowel ee
                    expected[n_block] = 1
                elif condition == '4':
                    # silence
                    expected[n_block] = 0
                elif condition == 0:
                    # silence
                    expected[n_block] = 0
            elif task == "Factorial":
                    if condition == '1':
                        #  "say 'la-leh-lee la-leh-lee'"
                        expected[n_block] = 2
                    elif condition == '2':
                        # "say 'la-leh-lee la-leh-lee' silently"
                        expected[n_block] = 0
                    elif condition == '3':
                        # "say 'ee-ee-ee ee-ee-ee'"
                        expected[n_block] = 1
                    elif condition == '4':
                        # "think 'la-leh-lee la-leh-lee'"
                        expected[n_block] = 0
                    elif condition == '0':
                        expected[n_block] = 0

    elif os.path.isfile(outfile):
        print('file already exists...')
        stored = pd.read_csv(outfile, sep='\t')
        print(stored)
        print('Continue at line ... \n(or enter 99 to stop)\n(or enter 55 to make new dataframe): ')
        answer = input('Continue at line ... (or enter 99): ')
        if answer == 99:
            print('quit.')
            quit()
        elif answer == 55:
            print('make new dataframe...')
            num_block = np.arange(1, n_blocks + 1, dtype=int)[:, np.newaxis]
            cond_list = np.zeros((n_blocks, 1), dtype=int)
            expected = np.zeros((n_blocks, 1), dtype=int)
            rec_types = np.zeros((n_blocks, 1))
            rec_types.fill(99)
            start_block = 0
        else:
            num_block = np.array(stored['num_block'])[:, np.newaxis]
            cond_list = np.array(stored['cond_list'])[:, np.newaxis]
            expected = np.array(stored['expected'])[:, np.newaxis]
            rec_types = np.array(stored['rec_types'])[:, np.newaxis]
            start_block = int(answer)

    for n_block in np.arange(start_block, n_blocks):
        # play the recording
        print('... playing ....')
        rec = glob.glob(folder + '/' + sub_row.id.lstrip("0") + '_' + str(n_block + 1) + '_*')[0]
        signal = np.frombuffer(wave.open(rec, 'r').readframes(-1), 'Int16')
        sd.play(signal)
        sd.wait()
        del signal
        # score what was heard and write information to dataframe
        print('block number: ' + str(n_block) + ', type:')
        rec_type = input('')
        rec_type = int(rec_type)
        rec_types[n_block] = rec_type
        df = pd.DataFrame(data=np.concatenate((num_block, cond_list, expected, rec_types), axis=1).astype(int), columns = ['num_block', 'cond_list', 'expected', 'rec_types'])
        df.to_csv(outfile, sep='\t', index=False)
        time.sleep(1)
